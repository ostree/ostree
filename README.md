Welcome to the ostree overlay

Here you can find some ebuilds for [G/P]entoo and Funtoo Linux.
Be carefull for use it! You must undestand what you do.

USAGE

emerge layman (if you don't have it yet) and:

1) Run:

    layman --overlays=http://github.com/ostree/ostree/raw/master/overlay.xml -L
    layman --overlays=http://github.com/ostree/ostree/raw/master/overlay.xml -a ostree


2) Edit /etc/layman/layman.cfg and add this: (second line)

    overlays  : http://www.gentoo.org/proj/en/overlays/layman-global.txt
                http://raw.github.com/ostree/ostree/master/overlay.xml


After this you can emerge everything from this overlay.
